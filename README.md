# Blender navigation

Replicates the blender scene numpad hot keys in Unity3d's scene editors

## How to install this package

Drag and drop the [BlenderNavigation folder](https://github.com/pflannery/unity-blender-navigation/tree/master/Assets/BlenderNavigation) to any unity project

## Hot keys

|Action|Key|
|------|---|
|Left (3d)|Ctrl Numpad 3|
|Right (3d)|Numpad 3|
|Back|Ctrl Numpad 1|
|Front|Numpad 1|
|Bottom|Ctrl Numpad 7|
|Top|Numpad 7|
|Left (2d)|Numpad 3|
|Right (2d)|Ctrl Numpad 3|
|--------------|--------|
|Orbit left|Numpad 4|
|Orbit right|Numpad 6|
|Orbit around left|Ctrl + Left Arrow|
|Orbit around right|Ctrl + Right Arrow|
|Orbit up|Numpad 8|
|Orbit down|Numpad 2|
|Orbit opposite|Numpad 9|
|--------------|--------|
|Roll left|Ctrl Numpad 5
|Roll right|Ctrl Numpad 6
|--------------|--------|
|Zoom in|Numpad +
|Zoom out|Numpad -
|Center on selected|Numpad period
|--------------|--------|
|2d/3d|Ctrl Numpad 5
|Perspective/Orthographic|Numpad 5
|--------------|--------|
|Global/Local|Numpad divide
|Pivot/Center|Ctrl Numpad divide
|--------------|--------|
|Grab\Move|G
|Rotate|R
|Scale|S
