﻿using UnityEditor;
using UnityEngine;

namespace BlenderNavigation.Editor
{

    internal class BlenderNavigationHandler
    {
        internal static bool IsEnabled { get; private set; }

        internal static void RegisterDelegates()
        {
            UnregisterDelegates();
            SceneView.onSceneGUIDelegate += OnSceneGUI;
            IsEnabled = true;
        }

        internal static void UnregisterDelegates()
        {
            SceneView.onSceneGUIDelegate -= OnSceneGUI;
            IsEnabled = false;
        }

        internal static void OnSceneGUI(SceneView sceneview)
        {
            Event e = Event.current;
            if (e.type == EventType.KeyDown)
            {
                HandleNavigationKeys(e, sceneview);
            }
        }

        internal static bool HandleNavigationKeys(Event e, SceneView sceneView)
        {
            var handledEvent = true;
            var cameraTransform = sceneView.camera.transform;
            var rotation = sceneView.rotation;
            var lastPivot = SceneView.lastActiveSceneView.pivot;
            var cameraLookRot = Quaternion.LookRotation(cameraTransform.forward, cameraTransform.up);

            //Debug.Log(System.String.Format("{0}, {1}, {2}", Event.current.keyCode,Event.current.control, Event.current.shift));

            switch (Event.current.keyCode)
            {
                case KeyCode.Keypad1:
                    var is3dFront = (e.control == false && sceneView.in2DMode == false);
                    var is2dFront = (e.control && sceneView.in2DMode);
                    var is3dBack = (e.control && sceneView.in2DMode == false);
                    var is2dBack = (e.control == false && sceneView.in2DMode);

                    if (is3dFront || is2dFront)
                        // front
                        sceneView.LookAtDirect(lastPivot, Quaternion.Euler(0, 180, 0));
                    else if (is3dBack || is2dBack)
                        // back
                        sceneView.LookAtDirect(lastPivot, Quaternion.Euler(0, 0, 0));
                    break;

                case KeyCode.Keypad7:
                    if (e.control == false)
                        // top
                        sceneView.LookAtDirect(lastPivot, Quaternion.Euler(90, 0, 0));
                    else
                        // bottom
                        sceneView.LookAtDirect(lastPivot, Quaternion.Euler(270, 0, 0));
                    break;

                case KeyCode.Keypad3:
                    if (e.control == false)
                        // right
                        sceneView.LookAtDirect(lastPivot, Quaternion.Euler(0, 270, 0));
                    else
                        // left
                        sceneView.LookAtDirect(lastPivot, Quaternion.Euler(0, 90, 0));
                    break;

                case KeyCode.Keypad5:
                    if (e.control == false)
                        // orthographic/perspective switching
                        sceneView.orthographic = !sceneView.orthographic;
                    else
                        // 2d/32 switching
                        sceneView.in2DMode = !sceneView.in2DMode;
                    break;

                case KeyCode.LeftArrow:
                    if (e.control)
                    {
                        // orbit left
                        sceneView.LookAtDirect(lastPivot, cameraLookRot * Quaternion.Euler(0, 15, 0));
                    }
                    break;

                case KeyCode.RightArrow:
                    if (e.control)
                    {
                        // orbit right
                        sceneView.LookAtDirect(lastPivot, cameraLookRot * Quaternion.Euler(0, -15, 0));
                    }
                    break;

                case KeyCode.Keypad4:
                    if (e.control == false)
                        // orbit with roll over poles left
                        sceneView.LookAtDirect(lastPivot, Quaternion.Euler(0, 15, 0) * cameraLookRot);
                    else
                        // roll left (unity already has shift for panning so swapping roll here)
                        sceneView.LookAtDirect(lastPivot, Quaternion.Euler(0, 0, -15) * rotation);

                    break;

                case KeyCode.Keypad6:
                    if (e.control == false)
                        // orbit with roll over poles right
                        sceneView.LookAtDirect(lastPivot, Quaternion.Euler(0, -15, 0) * cameraLookRot);
                    else
                        // roll right (unity already has shift for panning so swapping roll here)
                        sceneView.LookAtDirect(lastPivot, Quaternion.Euler(0, 0, 15) * rotation);
                    break;

                case KeyCode.Keypad8:
                    // orbit up
                    sceneView.LookAtDirect(lastPivot, rotation * Quaternion.Euler(15, 0, 0));
                    break;

                case KeyCode.Keypad2:
                    // orbit down
                    sceneView.LookAtDirect(lastPivot, rotation * Quaternion.Euler(-15, 0, 0));
                    break;

                case KeyCode.KeypadPlus:
                    // zoom in
                    sceneView.size /= 1.1f;
                    break;

                case KeyCode.KeypadMinus:
                    // zoom out
                    sceneView.size *= 1.1f;
                    break;

                // zoom to selected
                case KeyCode.KeypadPeriod:
                    if (Selection.transforms.Length == 1)
                        sceneView.LookAt(Selection.activeTransform.position, cameraTransform.rotation);
                    else if (Selection.transforms.Length > 1)
                    {
                        Vector3 tempVec = new Vector3();
                        for (int i = 0; i < Selection.transforms.Length; i++)
                        {
                            tempVec += Selection.transforms[i].position;
                        }
                        sceneView.LookAtDirect(tempVec / Selection.transforms.Length, cameraTransform.rotation);
                    }
                    break;

                // orbit opposite
                case KeyCode.Keypad9:
                    sceneView.LookAt(
                        lastPivot,
                        Quaternion.LookRotation(cameraTransform.forward, cameraTransform.up) * Quaternion.Euler(0, -180, 0),
                        sceneView.size,
                        sceneView.orthographic,
                        true
                    );
                    break;

                // global/local
                case KeyCode.KeypadDivide:
                    if (e.control == false)
                        Tools.pivotRotation = Tools.pivotRotation == PivotRotation.Global ? PivotRotation.Local : PivotRotation.Global;
                    else
                        Tools.pivotMode = Tools.pivotMode == PivotMode.Center ? PivotMode.Pivot : PivotMode.Center;
                    break;

                default:
                    handledEvent = false;
                    break;
            }

            return handledEvent;
        }
    }

}