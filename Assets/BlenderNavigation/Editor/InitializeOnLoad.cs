﻿using UnityEditor;

namespace BlenderNavigation.Editor
{
    [InitializeOnLoad]
    internal static class InitializeOnLoad
    {
        static InitializeOnLoad()
        {
            Register();
        }

        internal static void Register()
        {
            BlenderNavigationHandler.RegisterDelegates();
            BlenderTransformationHandler.RegisterDelegates();
        }

        internal static void Unregister()
        {
            BlenderNavigationHandler.UnregisterDelegates();
            BlenderTransformationHandler.UnregisterDelegates();
        }
    }
}