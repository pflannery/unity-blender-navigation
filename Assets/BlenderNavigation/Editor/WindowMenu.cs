﻿using UnityEditor;

namespace BlenderNavigation.Editor
{

    internal static class WindowMenu
    {
        [MenuItem("Window/Blender Navigation/Enable")]
        public static void OnEnableClick() => InitializeOnLoad.Register();

        [MenuItem("Window/Blender Navigation/Enable", true, 200)]
        public static bool VerifyEnableClick() => !BlenderNavigationHandler.IsEnabled;

        [MenuItem("Window/Blender Navigation/Disable")]
        public static void DisableClick() => InitializeOnLoad.Unregister();

        [MenuItem("Window/Blender Navigation/Disable", true, 200)]
        public static bool VerifyDisableClick() => BlenderNavigationHandler.IsEnabled;
    }

}