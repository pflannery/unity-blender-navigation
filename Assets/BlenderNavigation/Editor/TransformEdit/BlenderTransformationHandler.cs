﻿using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace BlenderNavigation.Editor
{
    public class BlenderTransformationHandler
    {
        internal static bool IsEnabled { get; private set; }

        internal static bool transformEditingEnabled = true;
        internal static bool snappingEnabledByDefault = false;

        internal static bool useTInstedOfR = false;

        internal static float translateOriginSize = 0.005f;
        internal static Color translateOriginColor = new Color(0.96f, 0.77f, 0, 1);

        static Translate translateEdit;
        static Rotate rotateEdit;
        static Scale scaleEdit;
        static ModalEdit activeModal;
        static ModalEdit delayStart;

        static bool swallowMouse = false;
        static int mouseButton;


        internal static void RegisterDelegates()
        {
            UnregisterDelegates();
            SceneView.onSceneGUIDelegate += OnSceneGUI;
            EditorApplication.hierarchyWindowItemOnGUI += OnHierarchyOnGUI;

            // Create our model edit singletons.
            translateEdit = new Translate();
            rotateEdit = new Rotate();
            scaleEdit = new Scale();

            IsEnabled = true;
        }

        internal static void UnregisterDelegates()
        {
            SceneView.onSceneGUIDelegate -= OnSceneGUI;
            EditorApplication.hierarchyWindowItemOnGUI += OnHierarchyOnGUI;

            translateEdit = null;
            rotateEdit = null;
            scaleEdit = null;

            IsEnabled = false;
        }

        internal static void OnSceneGUI(SceneView sceneView)
        {
            if (!BlenderTransformationHandler.transformEditingEnabled)
            {
                return;
            }

            if (Tools.viewTool == ViewTool.FPS || Tools.viewTool == ViewTool.Orbit)
            {
                return;
            }

            if (activeModal != null)
            {
                activeModal.Update();

                if (EditorWindow.focusedWindow != sceneView)
                {
                    // SceneView lost focus but we're in a mode so we force it back.
                    sceneView.Focus();
                }

                // We force the scene to continue to update if we are in a mode.
                HandleUtility.Repaint();
            }

            if (delayStart != null)
            {
                // We got a message to start!
                if (activeModal != null)
                {
                    activeModal.Cancel();
                }

                activeModal = delayStart;
                delayStart = null;
                activeModal.Start();
            }

            if (Event.current.isKey && Event.current.type == EventType.KeyDown && Event.current.control == false && Event.current.shift == false)
            {
                if (Event.current.keyCode == Preferences.TranslateKey)
                {
                    Event.current.Use();

                    if (activeModal != null)
                    {
                        activeModal.Cancel();
                    }

                    activeModal = translateEdit;
                    activeModal.Start();
                }
                else if (Event.current.keyCode == Preferences.RotateKey)
                {
                    Event.current.Use();

                    if (activeModal != null)
                    {
                        activeModal.Cancel();
                    }

                    activeModal = rotateEdit;
                    activeModal.Start();
                }
                if (Event.current.keyCode == Preferences.ScaleKey)
                {
                    Event.current.Use();

                    if (activeModal != null)
                    {
                        activeModal.Cancel();
                    }

                    activeModal = scaleEdit;
                    activeModal.Start();
                }
            }

            if (swallowMouse)
            {
                if (Event.current.button == mouseButton)
                {
                    if (Event.current.type == EventType.MouseUp)
                    {
                        swallowMouse = false;
                    }

                    Event.current.Use();
                }
            }
        }

        internal static void OnHierarchyOnGUI(int i, Rect r)
        {
            if (!BlenderTransformationHandler.transformEditingEnabled)
            {
                return;
            }

            if (Event.current.type == EventType.KeyDown)
            {
                if (Event.current.keyCode == Preferences.TranslateKey)
                {
                    SceneView.lastActiveSceneView.Focus();
                    Event.current.Use();

                    // Hey translate! We'll start it on next SceneGUI!
                    delayStart = translateEdit;
                }

                if (Event.current.keyCode == Preferences.RotateKey)
                {
                    SceneView.lastActiveSceneView.Focus();
                    Event.current.Use();
                    delayStart = rotateEdit;
                }

                if (Event.current.keyCode == Preferences.ScaleKey)
                {
                    SceneView.lastActiveSceneView.Focus();
                    Event.current.Use();
                    delayStart = scaleEdit;
                }
            }
        }

        internal static void ModalFinished()
        {
            activeModal = null;
        }

        internal static void SwallowMouseUntilUp(int button)
        {
            swallowMouse = true;
            mouseButton = button;
        }
    }
}