﻿using UnityEngine;

namespace BlenderNavigation.Editor
{

    public class Preferences
    {

        internal static bool EnableMouseConfirmCancel = true;

        internal static KeyCode TranslateKey = KeyCode.G;
        internal static KeyCode RotateKey = KeyCode.R;
        internal static KeyCode ScaleKey = KeyCode.S;

        internal static float TranslateSnapIncrement = 1;
        internal static float RotateSnapIncrement = 45;
        internal static float ScaleSnapIncrement = 1;

    }

}

